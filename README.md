# symfony-test

1) Create a docker-compose file which will run 3 containers: symfony application, mysql database and nginx server.
2) Create a symfony default application with flex architecture inside it's docker container which will be accessible on http://localhost.
3) Create a user entity 
4) Create a data fixture to load an admin user in database
5) Create an admin page which must be granted by a login form with username & password.
6) Use bootstrap for UI.
7) Once done, please make a pull request here.
