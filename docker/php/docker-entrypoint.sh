#!/bin/sh

export DOCKER_BRIDGE_IP=$(ip ro | grep default | cut -d' ' -f 3)

exec php-fpm

