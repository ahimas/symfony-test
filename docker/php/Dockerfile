FROM php:7.1-fpm-alpine

# https://getcomposer.org/doc/03-cli.md#composer-allow-superuser
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_VERSION 1.4.2
ENV PHP_INI_DIR /usr/local/etc/php
ENV PHP_CONFIG_DIR /usr/local/etc/php/conf.d
ENV SYMFONY_ENV dev
ENV CONF_PATH docker/php

RUN set -xe \
    && apk add --no-cache --virtual .build-deps $PHPIZE_DEPS openssl icu-dev zlib-dev \
    && docker-php-ext-install intl pdo_mysql zip \
	&& pecl install redis \

	# Install Composer
    && curl -s -f -L -o /tmp/installer.php https://raw.githubusercontent.com/composer/getcomposer.org/da290238de6d63faace0343efbdd5aa9354332c5/web/installer \
    && php -r " \
    \$signature = '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410'; \
    \$hash = hash('SHA384', file_get_contents('/tmp/installer.php')); \
    if (!hash_equals(\$signature, \$hash)) { \
        unlink('/tmp/installer.php'); \
        echo 'Integrity check failed, installer is either corrupt or worse.' . PHP_EOL; \
        exit(1); \
    }" \
    && php /tmp/installer.php --no-ansi --install-dir=/usr/bin --filename=composer --version=${COMPOSER_VERSION} \
    && rm /tmp/installer.php \
    && composer --ansi --version --no-interaction \

    # Install composer parallel downloads plugin
	&& composer global require "hirak/prestissimo:^0.3" --prefer-dist --no-progress --no-suggest --optimize-autoloader --classmap-authoritative \
	&& chown -R 82 /var/www/html \

	# Install PHPUnit
    && wget -qO- https://phar.phpunit.de/phpunit.phar > /usr/local/bin/phpunit \
    && chmod +x /usr/local/bin/phpunit \

    # Cleanup
    && apk del .build-deps \
	&& composer clear-cache \
	&& rm -rf \
        /var/cache/apk/* \
        /tmp/*

COPY ${CONF_PATH}/opcache.ini ${PHP_CONFIG_DIR}/10-opcache.ini
COPY ${CONF_PATH}/redis.ini ${PHP_CONFIG_DIR}/30-redis.ini
COPY ${CONF_PATH}/php-fpm.conf $PHP_INI_DIR/php-fpm.conf

#RUN set -xe \
#	&& pecl install xdebug-beta \
#	&& docker-php-ext-enable --ini-name 15-xdebug.ini xdebug

#COPY ${CONF_PATH}/xdebug.ini ${PHP_CONFIG_DIR}/15-xdebug.ini
COPY ${CONF_PATH}/docker-entrypoint.sh /usr/local/bin/

VOLUME /var/www/html
ENTRYPOINT "docker-entrypoint.sh"
